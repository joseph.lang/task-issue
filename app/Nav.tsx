"use client";
import Link from "next/link";
import React from "react";
import { usePathname } from "next/navigation";
import { signIn, signOut, useSession } from "next-auth/react";
const Nav = () => {
  const {status, data: session} = useSession();
  const currentPath = usePathname();
  const navLinks = [
    { href: "/", label: "Dashboard" },
    { href: "/issues", label: "Issues" },
  ];
  return (
    <div
      className="flex mb-5 px-4 md:px-5 h-20 items-center justify-between border-b-2 border-black max-w-screen gap-2
    "
    >
      <div className="flex items-center">
        <Link href="/" className="text-white text-xl hidden md:flex md:mr-6">TaskIssue</Link>
        <ul className="space-x-6 flex">
          {navLinks.map((navLink) => (
            <li key={navLink.href}>
              <Link
                className={`${currentPath === navLink.href ? "text-white border-b-2 border-blue-600" : "text-white"} hover:text-gray-300 transition-colors `}
                href={navLink.href}
              >
                {navLink.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      {status === 'loading' ? (
         <div className="skeleton h-[60px] w-[230px] rounded-lg"></div>
      ): (
        <div>
          {status === 'authenticated' && (
                  <button onClick={() => signOut()} className="btn bg-blue-500 text-white ">
                    Logout
                  </button>
          )}
          {status === 'unauthenticated' && (
                  <button onClick={() => signIn()}  className="btn bg-blue-500 text-white ">
                    Login
                  </button>
              )}
        </div>
      )}
    </div>
  );
};

export default Nav;

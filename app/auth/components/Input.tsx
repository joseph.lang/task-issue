import React from 'react'

interface Props {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  type: string;
  name?: string;
  value?: string;

}

const Input = ({onChange, title, type, name, value}: Props) => {
  return (
    <div className='flex gap-1 flex-col w-10/12 mt-5 text-black'>
    <label >{title}</label>
    <input className='p-3 border-slate-500 w-full border-2 rounded-md' placeholder={title} type={type} onChange={onChange} name={name} value={value} />
  </div>
  )
}

export default Input

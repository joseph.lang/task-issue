import React, { ReactNode } from 'react';

interface Props {
  title: string;
  children: ReactNode;
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void
  buttonTitle: string

}

const Form = ({ title, children, onSubmit, buttonTitle }: Props) => {
  return (
    <div className='flex justify-center items-center w-[300px] sm:w-screen '>
    <form className='w-[350px] md:w-[600px] lg:w-[900px] h-auto py-5 bg-white flex flex-col items-center rounded-md gap-3' onSubmit={onSubmit}>
    <h1 className='mt-10 text-black text-4xl'>{title}</h1>
    <div className='w-10/12 flex flex-col items-center'>
      {children}
    </div>
        <button className='px-7 py-3 bg-blue-500 text-black rounded-lg hover:bg-blue-700 hover:text-white transition-colors '>{buttonTitle}</button>
    </form>
  </div>
  )
}

export default Form

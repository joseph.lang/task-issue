'use client'
import React, { FormEvent, useEffect, useState } from 'react'
import Form from '../components/Form'
import Input from '../components/Input'
import { useRouter } from 'next/navigation'



const SignUpPage = () => {
  const router = useRouter()


  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
})
  const [emailExists, setEmailExists] = useState(false);

  const handleChange = (e: FormEvent<HTMLInputElement>) => {
    const input = e.currentTarget.name;
    const value = e.currentTarget.value;
    setFormData( {
      ...formData,
      [input]: value
    })
  }

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const url = 'http://localhost:3000/api/users/register'
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers : {
        'content-type': "application/json"
      }
    }
    await emailIsUnique()
    const res = await fetch(url, fetchOptions);
    if (res.ok) {
      router.push('/auth/signIn')
    }


  }
  const emailIsUnique = async () => {
    const url = "http://localhost:3000/api/users"
    const res = await fetch(url);
    if (res.ok) {
      const response = await res.json()
      for(let account of response) {
        if (account.email === formData.email) {
          setEmailExists(true)
          return;
        }
      }
      setEmailExists(false)
      return;
    }
  }

  return (
    <div className='w-screen flex justify-center'>
      <Form buttonTitle='Sign Up' title='Sign Up' onSubmit={onSubmit}>
        <Input title='First Name' type='text' onChange={handleChange} name="firstName" value={formData.firstName}/>
        <Input title='Last Name' type='text' onChange={handleChange} name="lastName" value={formData.lastName}/>
        <Input title='Email' type='text' onChange={handleChange} name="email" value={formData.email}/>
        {emailExists && (
          <p className='text-error' >This email already exists</p>
        )}
        <Input title='Password' type='password' onChange={handleChange} name="password" value={formData.password}/>
      </Form>
    </div>
  )
}

export default SignUpPage

'use client'
import React, { useState } from 'react'
import Form from '../components/Form';
import Input from '../components/Input';
import { signIn } from 'next-auth/react';
import Link from 'next/link';

const SignInPage = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

const onSubmit = (e: React.FormEvent) => {
  e.preventDefault();
  signIn('credentials', {
    email: email,
    password: password,
    redirect: true,
    callbackUrl: "/"
  });
};
  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.currentTarget.value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value);
  };
  return (
    <div className='flex justify-center items-center max-w-screen h-auto '>
      <Form buttonTitle='Sign In' title='Sign In' onSubmit={onSubmit}>
      <Input title='Email' onChange={handleEmailChange} type='text' />
      <Input title='Password' onChange={handlePasswordChange} type='password' />
      <Link href="/auth/signUp">
        <p className='text-black underline hover:no-underline my-4'>Don &apos;t have an account? Create one</p>
      </Link>
      </Form>
    </div>
  )
}

export default SignInPage

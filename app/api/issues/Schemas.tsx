import { z } from "zod";

export const issueSchema = z.object({
  title: z.string().max(255),
  description: z.string().max(65535),
});
export const putIssueSchema = z.object({
  title: z.string().max(255).optional(),
  description: z.string().max(65535).optional(),
  assignedToUserId: z.string().min(1).max(255).optional().nullable()

});

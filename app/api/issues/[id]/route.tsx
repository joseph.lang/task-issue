import { NextRequest, NextResponse } from "next/server";
import {  putIssueSchema } from "../Schemas";
import prisma from "@/prisma/client";
import { notFound } from "next/navigation";
import { getServerSession } from "next-auth";
import authOptions from "../../auth/authOptions";

interface Props {
  params: { id: string };
}

export async function PUT(request: NextRequest, { params }: Props) {
  const session = await getServerSession(authOptions);
  if (!session) {
    return NextResponse.json({}, {status:401})
  }

  const body = await request.json();
  const validation = putIssueSchema.safeParse(body);

  if (!validation.success) {
    return NextResponse.json(validation.error.errors, { status: 400 });
  }

  if (body.assignedToUserId) {
    const user = await prisma.user.findUnique({
      where: {id : body.assignedToUserId}
    })
  if (!user) {
    return NextResponse.json('User does not exist', {status: 400})
  }
  }

  const issue = await prisma.issue.findUnique({
    where: { id: parseInt(params.id) },
  });

  if (!issue) {
    return NextResponse.json("Issue does not exist", { status: 404 });
  }

  const updatedIssue = await prisma.issue.update({
    where: { id: issue.id },
    data: {
      title: body.title,
      description: body.description,
      status: body.status,
      assignedToUserId: body.assignedToUserId
    },
  });

  return NextResponse.json(updatedIssue);
}


export async function DELETE(request: NextRequest, {params}: Props) {
  const session = await getServerSession(authOptions);
  if (!session) {
    return NextResponse.json({}, {status:401})
  }

  const issue = await prisma.issue.findUnique({
    where: {id: parseInt(params.id)
  }});

  if (!issue) {
    notFound()
  }

  const deletedIssue = await prisma.issue.delete({
    where: {id: issue.id}
  })

  return NextResponse.json(deletedIssue)
}

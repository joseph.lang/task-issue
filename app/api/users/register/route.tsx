import prisma from "@/prisma/client";
import { NextRequest, NextResponse } from "next/server";
import {z} from "zod"
import bcrypt from "bcrypt"
const userSchema = z.object({
  firstName: z.string(),
  lastName:z.string(),
  email: z.string().email(),
  password: z.string().min(7)
})


export async function POST(request: NextRequest) {
  const body = await request.json();
  const validation = userSchema.safeParse(body);
  if (!validation.success) {
    return NextResponse.json(validation.error.errors, {status:400})
  };

  const user = await prisma.user.findUnique({where: {email: body.email}});

  if (user) {
    return NextResponse.json({error: "User already exists"}, {status:400})
  }
  const hashedPassword = await bcrypt.hash(body.password, 10);
  const newUser = await prisma.user.create({
    data: {
      firstName: body.firstName,
      lastName: body.lastName,
      email: body.email,
      hashedPassword
    }
  });
  return NextResponse.json({email: newUser.email})
}

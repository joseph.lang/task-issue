import prisma from '@/prisma/client'
import Link from 'next/link';
import React from 'react'
import Badge from './issues/components/Badge';

const LatestIssues = async () => {
  const issues = await prisma.issue.findMany({
    orderBy: {createdAt: 'desc'},
    where: {status: "OPEN"},
    take: 5
  });

  const allIssues = await prisma.issue.findMany()
  let openCount = 0
  let closedCount = 0
  let inProgressCount = 0
  for (let item of allIssues) {
    if (item.status === "OPEN") {
      openCount += 1
    } else if (item.status === "CLOSED") {
      closedCount += 1
    } else if(item.status === "IN_PROGRESS") {
      inProgressCount += 1
    }
   }
  return (

    <div className='flex gap-5 justify-center'>
      <div className="overflow-x-auto pl-5 pr-5">
        <table className="table">
      <thead>
        <tr>
        <th className='text-lg sm:text-2xl text-white'>Most Recent Open Issues</th>
        </tr>
      </thead>
      <tbody>
        {issues.map((issue) => (
          <tr key={issue.id}>
            <td>
              <div className='flex justify-between'>
                <div className='flex flex-col gap-2'>
                <Link className='text-white border-b-2 border-gray-500 hover:border-blue-500' href={`issues/${issue.id}`}>
                {issue.title}
                </Link>
                <Badge status={issue.status}/>
                </div>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
        </table>
      </div>
      <div className='hidden md:block pr-5'>
        <h1 className='text-2xl text-white font-bold mb-2 pt-[15px] pb-[15px] pl-[20px] pr-[20px]'>
          Summary
        </h1>
        <div className='flex flex-col gap-2'>
          <div className='flex gap-2 items-center pl-[20px] '>
            <Badge status='OPEN' />
            <p className='text-lg font-bold'>{openCount}</p>
          </div>
          <div className='flex gap-2 pl-[20px]'>
            <Badge status='IN_PROGRESS' />
            <p className='text-lg font-bold'>{inProgressCount}</p>
          </div>
          <div className='flex gap-2 pl-[20px]'>
          <Badge status='CLOSED' />
            <p className='text-lg font-bold'>{closedCount}</p>
          </div>
          </div>
        </div>
    </div>

  )
}

export default LatestIssues

'use client'
import React, { useEffect, useState } from "react";
import Link from "next/link";
import Badge from "./components/Badge";
import { Issue, Status } from "@prisma/client";
import IssuesSkeleton from "./components/IssuesSkeleton";
export const statuses: {label: string, value?: Status}[] = [
  {label: 'All'},
  {label: 'Open', value:'OPEN'},
  {label: 'In Progress', value:'IN_PROGRESS'},
  {label: 'Closed', value:'CLOSED'},

]
const IssuesPage =  () => {

  const [issues, setIssues] = useState<Issue[]>([])
  const [filteredIssues, setFilteredIssues] = useState<Issue[]>()
  const [isLoading, setIsLoading] = useState(true)
  const getIssues = async () => {
    try {
      const url = "http://localhost:3000/api/issues";
      const req = await fetch(url);
      if (req.ok) {
        const issueList = await req.json();
        setIssues(issueList);
        setFilteredIssues(issueList)
        setIsLoading(false)
      }
    } catch (error) {
      console.error('Error fetching issues:', error);
    }
  }

  useEffect(() => {
    getIssues();
  }, []);



  const handleChange =  (event: React.ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault();
    const value = event.target.value
    console.log(value)
    if (value === "All") {
      setFilteredIssues(issues)
    } else {
      const filterIssues = issues.filter((issue) => issue.status === value)
      setFilteredIssues(filterIssues)
    }
  }

  return (
    <>
      {isLoading ?
        <IssuesSkeleton /> :
        <div className="flex flex-col pl-5 pr-5">
        <div className="flex space-x-6">
          <div>
            <Link href="/issues/new">
              <button className="btn btn-secondary max-w-32">New Issue</button>
            </Link>
          </div>
          <select onChange={handleChange}  className="select select-bordered w-full max-w-xs">
           <option disabled value="">Filter by status...</option>
              {statuses.map((status) => (
           <option key={status.value} value={status.value}>
              {status.label}
           </option>
        ))}
        </select>
        </div>
      <div className="overflow-x-auto mt-5">
        <table className="table">
          <thead>
            <tr>
              <th className="md:text-center text-white text-xl">Title</th>
              <th className="text-center text-white text-xl">Status</th>
              <th className="md:text-center hidden md:table-cell text-white text-xl">
                Created At
              </th>
            </tr>
          </thead>
          <tbody>
            {filteredIssues?.length === 0 ? <tr><td><p>No results</p></td></tr>: (
               filteredIssues?.map((issue) => (
                <tr key={issue.id} >
                  <td className="md:text-center max-w-80">
                    <Link
                      className="border-b-2 border-gray-500 text-white hover:border-blue-600"
                      href={`issues/${issue.id}`}
                    >
                      {issue.title}
                    </Link>
                  </td>
                  <td className="md:text-center table-cell text-white">
                    <Badge status={issue.status} />
                  </td>
                  <td className="md:text-center hidden md:table-cell text-white">
                  {issue.createdAt &&
                    new Date(issue.createdAt).toLocaleDateString('en-US', {
                      weekday: 'long',
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric'
                    })
                  }
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
    </div>
      }
    </>
  );
};

export default IssuesPage;

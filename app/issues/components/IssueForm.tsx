"use client";
import React, { useState } from "react";
import { useRouter } from "next/navigation";
import { Issue } from "@prisma/client";
import { Status } from "@prisma/client";
interface Props {
  issue?: Issue;
}

const IssueForm = ({ issue }: Props) => {
  const router = useRouter();
  const [formData, setFormData] = useState({
    title: issue?.title || "",
    description: issue?.description || "",
    status: issue?.status || Status.OPEN
  });

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>,
  ) => {
    const name = event.target.name;
    const value = event.target.value;

    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      if (issue) {
        const url = `http://localhost:3000/api/issues/${issue.id}`;
        const fetchOptions = {
          method: "PUT",
          body: JSON.stringify(formData),
          headers: {
            "Content-type": "application/json",
          },
        };
        const req = await fetch(url, fetchOptions);
        if (req.ok) {
          router.push(`/issues/${issue.id}`);
          router.refresh();
        }
      } else {
        const url = "http://localhost:3000/api/issues";
        const fetchOptions = {
          method: "POST",
          body: JSON.stringify(formData),
          headers: {
            "Content-type": "application/json",
          },
        };
        const req = await fetch(url, fetchOptions);
        if (req.ok) {
          router.push("/issues");
          router.refresh();
        }
      }
    } catch (error) {
      console.error("There was an error", error);
    }
  };

  return (
    <div className="flex justify-center items-center">
      <form
        onSubmit={handleSubmit}
        className="max-w-4xl flex flex-col items-start gap-5 mt-5 w-full"
      >
        <label className="input input-bordered flex items-center gap-2 w-full text-sm">
          <input
            value={formData.title}
            name="title"
            type="text"
            className="w-full"
            placeholder="Title"
            onChange={handleChange}
            required
          />
        </label>
        <textarea
          value={formData.description}
          name="description"
          className="textarea textarea-bordered h-32 text-md resize-none w-full"
          placeholder="Description"
          onChange={handleChange}
          required
        ></textarea>
        {issue && (
        <select defaultValue={issue?.status || Status.OPEN} name="status" onChange={handleChange} value={formData.status} className="select select-bordered w-full max-w-xs">
          <option value={Status.OPEN}>Open</option>
          <option value={Status.IN_PROGRESS}>In Progress</option>
          <option value={Status.CLOSED}>Closed</option>
        </select>
        )}
        {issue ? (
          <div className="flex gap-2">
          <button className="btn btn-primary">Save</button>
          <button onClick={() => router.push(`/issues/${issue.id}`)} className="btn btn-neutral">Cancel</button>
          </div>

        ) : (
          <button className="btn btn-primary">Create Issue</button>
        )}
      </form>
    </div>
  );
};

export default IssueForm;

import React from 'react'

const IssuesSkeleton = () => {

  return (
<div className='flex flex-col'>
  <div className='flex gap-5'>
    <div className="skeleton h-14 w-28 ml-5"></div>
    <div className="skeleton h-14 w-80"></div> {/* Closing tag added */}
  </div>
  <div className='flex flex-col gap-3 items-center mt-4'>
    <div className="skeleton h-8 w-10/12"></div>
    <div className="skeleton h-8 w-10/12"></div>
    <div className="skeleton h-8 w-10/12"></div>
    <div className="skeleton h-8 w-10/12"></div>
    <div className="skeleton h-8 w-10/12"></div>
  </div>
</div>

  )
}

export default IssuesSkeleton

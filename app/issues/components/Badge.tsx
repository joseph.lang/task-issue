import { Status } from "@prisma/client";
import React from "react";

interface Props {
  status: Status;
}

const statusMap: Record<Status, { label: string; color: string }> = {
  OPEN: { label: "Open", color: "badge-error" },
  IN_PROGRESS: { label: "In Progress", color: "badge-info" },
  CLOSED: { label: "Closed", color: "badge-accent" },
};

const Badge = ({ status }: Props) => {
  return (
    <div className={`badge ${statusMap[status].color}`}>
      {statusMap[status].label}
    </div>
  );
};

export default Badge;

'use client'
import { Issue, User } from '@prisma/client';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
interface Props {
  issueId: number;
  issue: Issue;
}

const AssigneeDropdown: React.FC<Props> = ({ issueId, issue }: Props) => {
  const [users, setUsers] = useState<User[]>([]);
  const [assignee, setAssignee] = useState<string>("");
  const [loading, setIsLoading] = useState<Boolean>(true);

  const notify = () => toast("Issue successfully reassigned!", {
    position: "top-right",
    autoClose: 4000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "dark",
  });

  useEffect(() => {
    const getUsers = async () => {
      const url = 'http://localhost:3000/api/users/';
      const req = await fetch(url);
      if (req.ok) {
        const userList: User[] = await req.json();
        setUsers(userList);
        setIsLoading(false)
      }
    };

    const fetchAssignee = async () => {
      if (issue.assignedToUserId !== null) {
        setAssignee(issue.assignedToUserId.toString());
      } else {
        setAssignee("");
      }
    };

    getUsers();
    fetchAssignee();
  }, [issue]);

  const handleAssigneeChange = async (event: React.ChangeEvent<HTMLSelectElement>) => {
    event.preventDefault();
    const value = event.target.value;

    try {
      let requestBody;
      if (value === "") {
        requestBody = { assignedToUserId: null };
      } else {
        requestBody = { assignedToUserId: value };
      }

      const url = `http://localhost:3000/api/issues/${issueId}`;
      const fetchOptions = {
        method: "PUT",
        body: JSON.stringify(requestBody),
        headers: {
          'Content-type': 'application/json'
        }
      };
      const req = await fetch(url, fetchOptions);
      if (req.ok) {
        setAssignee(value);
        notify();
      }
    } catch (error) {
      console.error('An error occurred', error);
    }
  };

  return (
    <>
    {loading ? <div className="skeleton h-[60px] w-80 rounded-lg"></div> :
    <select defaultValue={assignee} value={assignee} onChange={handleAssigneeChange} className="select select-bordered w-full max-w-xs">
      <option disabled value="">Assign...</option>
      <option value="">Unassign</option>
      {users?.map((user) => (
        <option key={user.id} value={user.id.toString()}>{user.name? user.name : user.firstName! + " " + user.lastName!}</option>
      ))}
    </select>
  }
  </>
  );
}

export default AssigneeDropdown;

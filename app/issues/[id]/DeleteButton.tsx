'use client'
import React, { useState } from 'react'
import { FaRegTrashCan } from "react-icons/fa6";
import { useRouter } from 'next/navigation';
import Modal from '@/app/components/Modal';

interface Props {
  issueId: number

}


const DeleteButton = ({ issueId }: Props) => {
  const router = useRouter()
  const [showAlert, setAlert] = useState(false)


  const handleAlert = () => {
      setAlert(!showAlert)

  }

  const handleDelete = async () => {
    try {
      const url = `http://localhost:3000/api/issues/${issueId}`;
      const fetchOptions = {
        method: "DELETE",
        headers : {
          'Content-type': "application/json"
        }
      };
      const req = await fetch(url, fetchOptions);
      if (req.ok) {
        router.push("/issues");
        router.refresh();
      }
    } catch (error) {
      console.error("An error occurred", error);
    }
  };


  return (
    <>
    {showAlert && (
      <Modal title="Are you sure you want to delete this issue?" handleCancel={handleAlert} handleConfirm={handleDelete} />
    )}
    <button onClick={handleAlert} className="btn btn-error w-28 mt-3">
      Delete <FaRegTrashCan />
    </button>
    </>
  );
};

export default DeleteButton;

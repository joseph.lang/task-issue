import prisma from "@/prisma/client";
import { notFound } from "next/navigation";
import React from "react";
import Badge from "../components/Badge";
import EditButton from "./EditButton";
import DeleteButton from "./DeleteButton";
import { getServerSession } from "next-auth";
import authOptions from "@/app/api/auth/authOptions";
import AssigneeDropdown from "./AssigneeDropdown";
interface Props {
  params: { id: string };
}

const IssueDetailPage = async ({ params }: Props) => {
  const session = await getServerSession(authOptions)
  const issue = await prisma.issue.findUnique({
    where: { id: parseInt(params.id) },
  });
  if (!issue) {
    notFound();
  }

  return (
    <div className="flex items-start justify-center w-screen">
      <div className="flex flex-col w-9/12 lg:max-w-4xl">
        <div className="flex justify-between">
          <h1 className="text-2xl">{issue.title}</h1>
          {session && (
            <AssigneeDropdown issueId={issue.id} issue={issue}/>

          )}
        </div>
        <div className="flex gap-2 mt-2">
          <Badge status={issue.status} />
          <p>{issue.createdAt.toDateString()}</p>
        </div>
        <div className="block w-full rounded-lg bg-white text-left text-surface shadow-secondary-1 border-2 border-slate-400 mt-3">
          <div className="p-3">
            <p className="text-black">{issue.description}</p>
          </div>
        </div>
        {session && (
          <div className="flex gap-2">

              <EditButton issueId={issue.id} />
              <DeleteButton issueId={issue.id} />

          </div>
        )}
      </div>
    </div>
  );
};

export default IssueDetailPage;

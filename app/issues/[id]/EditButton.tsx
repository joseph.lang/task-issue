import Link from "next/link";
import React from "react";
import { FaRegEdit } from "react-icons/fa";

interface Props {
  issueId: number;
}

const EditButton = ({ issueId }: Props) => {
  return (
    <Link href={`/issues/${issueId}/edit`}>
      <button className="btn btn-warning w-28 mt-3">
        Edit
        <FaRegEdit />
      </button>
    </Link>
  );
};

export default EditButton;

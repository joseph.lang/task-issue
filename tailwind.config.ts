import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  daisyui: {
    themes: [
      {
        lightTheme: {
          primary: "#00c5f8",

          secondary: "#009900",

          accent: "#00cf00",

          neutral: "#1f1f1f",

          "base-100": "#232b2b",

          info: "#0097ff",

          success: "#00be83",

          warning: "#ff9400",

          error: "#e5405b",
        },
      }
    ],
  },
  plugins: [require("daisyui")],
};
export default config;

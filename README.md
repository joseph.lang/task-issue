# TaskIssue
TaskIssue is a way for developers to create, read, update and delete issues. Issues can be assigned to developers by other developers and they can be marked as In Progress, Open, or Closed. A user must be logged in via Google in order to assign, create, edit, or delete issues.

## How to get started
1. install git, node, and mysql on your device
2. Fork the repository
3. Clone the repository via http in your terminal in your desired directory
4. Create a .env file in root project directory
5. Add ```DATABASE_URL="mysql://<User>:<Password>&h@localhost:3306/<mydb>"``` to .env file with your mysql credentials
6. Run ```npm i```, ```npx prisma migrate dev``` and  ```npm run dev``` in your terminal
7. You are setup!

## API Documentation

### Issues
| Action | Method | URL |
| ------ | ------ | --- |
|[Get all issues](#get-all-issues) | GET | http://localhost:3000/api/issues |
|[Create an issue](#create-an-issue) | POST | http://localhost:3000/api/issues |
|[Update an issue](#update-an-issue) | PUT | http://localhost:3000/api/issues/issueId |
|[Delete an issue](#delete-an-issue) | DELETE | http://localhost:3000/api/issues/issueId |

### Users
| Action | Method | URL |
| ------ | ------ | --- |
|[Get all users](#get-all-users) | GET | http://localhost:3000/api/users |
|[Create a user](#create-a-user) | POST | http://localhost:3000/api/users/register |

#### Get all issues
##### Response:
```
[
	{
		"id": 1,
		"title": "issue 1",
		"description": "description 1",
		"status": "OPEN",
		"createdAt": "2024-03-27T03:21:35.411Z",
		"updatedAt": "2024-03-28T03:57:26.214Z",
		"assignedToUserId": "hjdnfhgb983njdghruH4kdlB2"
	},
]
```
#### Create an issue
##### Request body:
```
{
	"title": "issue1",
	"description": description of issue1
}
```
##### Response:
```
{
	"id": 2,
	"title": "issue22",
	"description": "description of issue22",
	"status": "OPEN",
	"createdAt": "2024-04-10T04:38:51.046Z",
	"updatedAt": "2024-04-10T04:38:51.046Z",
	"assignedToUserId": null
}
```
#### Update an issue
##### Request body:
```
{
  "title": "issue19" (optional),
  "description": "Description of issue19" (optional),
  "assignedToUserId": "hjdnfhgb983njdghruH4kdlB2" (optional)
}
```
##### Response:
```
{
  "id": 19
  "title": "issue19",
  "description": "Description of issue19",
  "status: "OPEN",
  "createdAt": "2024-03-27T03:21:35.411Z",
	"updatedAt": "2024-03-28T01:20:16.502Z",
  "assignedToUserId": "hjdnfhgb983njdghruH4kdlB2",
}
```
#### Delete an issue
##### Response:
```
{
  "id": 19
  "title": "issue19",
  "description": "Description of issue19",
  "status: "OPEN",
  "createdAt": "2024-03-27T03:21:35.411Z",
	"updatedAt": "2024-03-28T01:20:16.502Z",
  "assignedToUserId": "hjdnfhgb983njdghruH4kdlB2",
}
```
#### Get all users
##### Response:
```
[
	{
		"id": "hjdnfhgb983njdghruH4kdlB2",
		"name": "Bob Doe",
		"email": "bod@gmail.com",
		"emailVerified": null,
		"image": "https://image-url"
	}
]
```
#### Create a user
##### Request body:
```
{
	"firstName": "Bob",
	"lastName": "Hill",
	"email": "bob1@gmail.com",
	"password": "password"
}
```
##### Response:
```
{
	"email": "bob1@gmail.com"
}
```
